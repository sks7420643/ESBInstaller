package Installer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Scanner;

@SpringBootApplication
public class Application {

    @Autowired
    private InstallerService installerService;

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            logger.warn("Please create POSTGRES and JAVA_KEYSTORE environment variables. Have you done this [y/n]?");
            Scanner scanner = new Scanner(System.in);
            String answer = scanner.nextLine();
            scanner.close();
            if(!answer.equals("y") && !answer.equals("yes")) {
                throw new Exception("You MUST create environment variables POSTGRES and JAVA_KEYSTORE " +
                        "before installation!!!");
            }
            SpringApplication.run(Application.class, args);
            logger.info("ESB installed");
        } catch (Exception exception) {
            logger.error("ESB doesn't installed Exception: " + exception.getMessage());
        }
    }

    @Bean
    public String initInstall() throws Exception {
        logger.info("Init ESBAdmin user creation");
        installerService.addAdmin();
        logger.info("Init system variables creation");
        installerService.addVars();
        return "OK";
    }
}